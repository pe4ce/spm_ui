import Routes from 'routes';
import { StoreProvider } from 'store';

import 'react-datepicker/dist/react-datepicker.css';
import 'react-calendar/dist/Calendar.css';

import './styles/calendar.scss';

function App() {
  return (
    <StoreProvider>
      <Routes />
    </StoreProvider>
  );
}

export default App;
