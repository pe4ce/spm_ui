import { useState } from "react";
import { Button, Card } from "components/atoms";
import { MobileLayout, FormLayout } from "components/layout";
import { HeaderSearch, HeaderSelected, Input } from "components/molecules";
import { InputNumberButton } from "components/molecules";
import { CardPabrik } from "components/organism";
import { DrawerSelectDate, DrawerSelectInput } from "components/drawer";

import { ReactComponent as PlusSVG } from "assets/icons/plus-icon.svg";
import { ReactComponent as MinusSVG } from "assets/icons/minus-icon.svg";

import styles from "./styles.module.scss";

const {
  root,
  form_root,
  form_container,
  list_container,
  list_container_content_list,
  input_container,
  detail_title,
  input_range,
  qualityContainer,
} = styles;

const FormQualityControl = (props) => {
  const {
    data = [
      {
        nama: "Pabrik A",
        perusahaan: "Asian Agri",
        alamat:
          "Jl. M.H. Thamrin No.31, RT.4/RW.1, Gondangdia, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10230",
      },
      {
        nama: "Pabrik B",
        perusahaan: "PT. Pasifik Agro Sentosa",
        alamat:
          "Sudirman Central Business District, RT.5/RW.3, Senayan, Kebayoran Baru, South Jakarta City, Jakarta 1219",
      },
    ],
  } = props;
  const [selected, setSelected] = useState({
    nama: "",
    perusahaan: "",
    alamat: "",
  });
  const [productType, setProductType] = useState(-1);
  const [searchValue, setSearchValue] = useState("");
  const { onChange = () => {} } = props;

  const FormCPO = (
    <div className={form_container} style={{ marginBottom: "10rem" }}>
      <p>
        <h4>CPO</h4>
      </p>

      <DrawerSelectDate label="Tanggal" placeholder="Pilih tanggal" />

      <div className={input_container}>
        <InputNumberButton label="Total Item" satuan="/kg" />
      </div>
      <h5 className={detail_title}>Detail Parameter</h5>

      <div className={input_container}>
        <InputNumberButton label="FFA Palmintic CPO" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="Moisture CPO" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="DIRT CPO" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="DOBI CPO" satuan="%" />
      </div>
    </div>
  );

  const FormKernel = (
    <div className={form_container} style={{ marginBottom: "10rem" }}>
      <p>
        <h4>Kernel</h4>
      </p>

      <DrawerSelectDate label="Tanggal" placeholder="Pilih tanggal" />

      <div className={input_container}>
        <InputNumberButton label="Total Item" satuan="/kg" />
      </div>
      <h5 className={detail_title}>Detail Parameter</h5>

      <div className={input_container}>
        <InputNumberButton label="FFA Palmintic IKS" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="Moisture IKS" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="DIRT IKS" satuan="%" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="BROKEN IKS" satuan="%" />
      </div>
    </div>
  );

  const FormCangkang = (
    <div className={form_container} style={{ marginBottom: "10rem" }}>
      <p>
        <h4>Kernel</h4>
      </p>
      <DrawerSelectDate label="Tanggal" placeholder="Pilih tanggal" />
      <div className={input_container}>
        <InputNumberButton label="Total Item" satuan="/kg" />
      </div>
      <div className={input_container}>
        <label>
          <div className={qualityContainer}>
            <p>Quality Rate</p>
            <div style={{ width: 100 }}>
              <Input value="0" center={true} />
            </div>
          </div>
          <input
            type="range"
            className={input_range}
            max="10"
            min="0"
            value="0"
          />
        </label>
      </div>
    </div>
  );
  const formComponent = [FormCPO, FormKernel, FormCangkang];
  return (
    <MobileLayout>
      {selected.nama !== "" ? (
        <section className={root}>
          <HeaderSelected
            title="Quality Control"
            nama={selected?.perusahaan}
            alamat={selected?.alamat}
            onChange={() => {
              setSelected({ nama: "", perusahaan: "", alamat: "" });
            }}
            onOk={() => {}}
          />
          <section className={form_root}>
            <div className={form_container} style={{ marginBottom: "2rem" }}>
              <DrawerSelectInput
                label="Tempat Penyimpanan"
                placeholder="Pilih tempat penyimpanan"
                title="Tempat Penyimpanan"
                selections={[
                  {
                    label: "OST 1",
                  },
                  {
                    label: "OST 2",
                  },
                  {
                    label: "OST 3",
                  },
                ]}
                positionStatic
              />

              <DrawerSelectInput
                label="Tipe Produk"
                placeholder="Pilih tipe produk"
                title="Pilih Produk"
                selections={[
                  {
                    label: "CPO",
                  },
                  {
                    label: "Kernel",
                  },
                  {
                    label: "Cangkang",
                  },
                ]}
                positionStatic
                onChange={(va) => setProductType(va)}
              />
            </div>
            {productType > -1 && formComponent[productType]}
          </section>
        </section>
      ) : (
        <section className={root}>
          <HeaderSearch
            title="Quality Control"
            placeholder="Find factory location"
            onChange={(e) => setSearchValue(e)}
            value={searchValue}
          />

          <article className={list_container}>
            <ul className={list_container_content_list}>
              {data
                ?.filter((dt) =>
                  dt.nama.toLowerCase().includes(searchValue.toLowerCase())
                )
                .map((item, key) => {
                  return (
                    <li
                      key={key}
                      onClick={() => {
                        onChange(key);
                        setSelected(item);
                      }}
                    >
                      {item.nama}
                    </li>
                  );
                })}
            </ul>
          </article>
        </section>
      )}
    </MobileLayout>
  );
};

export default FormQualityControl;
