import { useHistory } from 'react-router';

import { Button } from 'components/atoms';

import styles from './styles.module.scss';

const { root, content } = styles;

const FormFooter = (props) => {
  const {
    okText,
    onOk,
    cancelText,
    currentStep,
    steps,
  } = props;
  const history = useHistory();

  return (
    <footer className={root}>
      <div className={content}>
        <>
          <div>
            <Button
              variant="secondary"
              onClick={() => { history.goBack() }}
            >
              {cancelText || 'Cancel'}
            </Button>
          </div>
          {currentStep < steps - 1 ? (
            <div style={{ width: '100%' }}>
              <Button variant="primary" onClick={() => onOk()} size="full">
                Next
              </Button>
            </div>
          ) : (
            <div style={{ width: '100%' }}>
              <Button variant="primary" onClick={() => onOk()} size="full">
                {okText || 'Save'}
              </Button>
            </div>
          )}

        </>
      </div>
    </footer >
  );
};

export default FormFooter;
