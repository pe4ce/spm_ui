import { useHistory } from 'react-router';
import { Button, Card, Divider } from 'components/atoms';
import { MobileLayout } from 'components/layout';

import { ReactComponent as ChevronRightSVG } from 'assets/icons/chevron-right-icon.svg';

import styles from './styles.module.scss';
import { FormCard } from 'components/organism';

const {
  root,
  page_header,
  list_container,
  form_card,
  form_card__action,
  form_card__action_name,
  form_card__information,
  form_card__icon,
} = styles;

const FormListPage = () => {
  const history = useHistory();

  return (
    <MobileLayout>
      <section className={root}>
        <header className={page_header}>
          <h1>SPM Form</h1>
        </header>
        <article className={list_container}>
          <FormCard title="Penerimaan Bahan Baku" lastSubmit="3 minutes ago" action={() => history.push('/form/penerimaan-bahan-baku')} />
          <FormCard title="Output Pabrik" lastSubmit="25 minutes ago" action={() => history.push('/form/output-pabrik')} />
          <FormCard title="Quality Control" lastSubmit="9 hours ago" action={() => history.push('/form/quality-control')} />
          <FormCard title="Mill Check" lastSubmit="2 days ago" />
          <FormCard title="Delivery Order" lastSubmit="7 days ago" action={() => history.push('/form/delivery-order')} />
        </article>
      </section>
    </MobileLayout>
  );
};

export default FormListPage;
