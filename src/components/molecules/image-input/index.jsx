import React, { useState } from "react";
import styler from "./styles.module.scss";

const { buttons } = styler;
const App = () => {
  const [selectedImage, setSelectedImage] = useState();

  // This function will be triggered when the file field change
  const imageChange = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      setSelectedImage(e.target.files[0]);
    }
  };

  // This function will be triggered when the "Remove This Image" button is clicked
  const removeSelectedImage = () => {
    setSelectedImage();
  };

  return (
    <>
      <div style={styles.container}>
        {!selectedImage && (
          <React.Fragment>
            <label className={buttons} for="input">
              Ambil Foto
            </label>
            <input
              id="input"
              style={styles.input}
              accept="image/*"
              type="file"
              onChange={imageChange}
            />
          </React.Fragment>
        )}

        {selectedImage && (
          <div style={styles.preview}>
            <img
              src={URL.createObjectURL(selectedImage)}
              style={styles.image}
              alt="Thumb"
            />
            <button onClick={removeSelectedImage} style={styles.delete}>
              X
            </button>
          </div>
        )}
      </div>
    </>
  );
};

export default App;

// Just some styles
const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    display: "none",
  },
  preview: {
    position: "relative",
  },
  image: { maxWidth: "100%", maxHeight: 320, borderRadius: 13 },
  delete: {
    position: "absolute",
    top: 5,
    right: 5,
    cursor: "pointer",
    width: 25,
    height: 25,
    fontWeight: "700",
    borderRadius: 50 + "%",
    borderWidth: 1,
    borderColor: "gray",
    boxShadow: "-1px 1px 3px #7a8598",
  },
};
