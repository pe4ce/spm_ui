import styles from './styles.module.scss';
import { PageNavigationTitle } from 'components/molecules';
import { CardPabrik } from 'components/organism';
import FormFooter from '../../layout/form/footer';

const { root, container, header, root_search, content } = styles;

const HeaderSelected = (props) => {
    const {
        children,
        title,
        nama,
        alamat,
        onChange = () => { },
        secondActionElement = '',
        okText,
        onOk,
        cancelText,
    } = props;

    return (
        <section className={root}>
            <section className={container}>
                <header className={header}>
                    <div>
                        <PageNavigationTitle
                            title={title}
                            onBack={onChange}
                            secondActionElement={secondActionElement}
                            small
                        />
                    </div>
                    <div className={root_search}>
                        <CardPabrik
                            title={nama}
                            alamat={alamat}
                            color="gray"
                            icon_color="#EF6D3F"
                            change={onChange}
                            selected />
                    </div>
                </header>
                <FormFooter
                    okText={okText}
                    onOk={onOk}
                    cancelText={cancelText}
                />
            </section>
        </section>

    );
};

export default HeaderSelected;
