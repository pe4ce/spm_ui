import { useHistory } from 'react-router';

import styles from './styles.module.scss';

import { ReactComponent as LeftSVG } from 'assets/icons/chevron-left-icon.svg';

const { root, icon } = styles;

const PageNavigationTitle = (props) => {
  const history = useHistory();
  const { title = '', onBack = null, secondActionElement = '', small } = props;

  return (
    <header className={root}>
      {onBack ? (
        <div className={icon} onClick={() => { history.goBack() }}>
          <LeftSVG />
        </div>
      ) : (
        <div />
      )}
      {small ? <h4 style={{ fontSize: '2rem' }}>{title}</h4> : <h3>{title}</h3>}
      {secondActionElement ? (
        <div>{secondActionElement}</div>
      ) : (
        <div className={icon}></div>
      )}
    </header>
  );
};

export default PageNavigationTitle;
