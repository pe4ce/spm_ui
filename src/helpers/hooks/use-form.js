import { useState } from 'react';

const useForm = (initialState) => {
  const [state, setState] = useState(initialState);

  const handleStateSchange = (key, value) => {
    setState((prev) => {
      return {
        ...prev,
        [key]: value,
      };
    });
  };

  return {
    state,
    handleStateSchange,
  };
};

export default useForm;
