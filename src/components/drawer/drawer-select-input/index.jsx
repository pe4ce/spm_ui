import { DrawerBase, Input } from 'components/molecules';
import { useState } from 'react';

import { ReactComponent as RightSVG } from 'assets/icons/chevron-right-icon.svg';
import { ReactComponent as SearchSVG } from 'assets/icons/search-icon.svg';

import styles from './styles.module.scss';

const { root, content_list, content_search } = styles;

const DrawerSelectInput = (props) => {
  const [selected, setSelected] = useState({ label: '', value: '' }); // {label: "", value: ""}
  const {
    selections = [],
    label = '',
    placeholder = '',
    onChange = () => { },
    value = selected?.label,
    positionStatic = false,
    withSearch = false,
    disabled = false,
    title = '',
    suffixIcon = '',
    searchPlaceholder = '',
    withBack = false,
    withConfirm = false
  } = props; // selections = [{label: "", value: ""}]
  const [show, setShow] = useState(false);
  const [searchValue, setSearchValue] = useState('');

  return (
    <div>
      {selected.value !== '' ? (
        <Input
          type="text"
          value={value || null}
          placeholder={placeholder || 'Select'}
          label={label || ''}
          readonly
          onClick={() => setShow((prev) => !prev)}
          suffixIcon={suffixIcon ||
            <div
              style={{
                display: 'grid',
                placeContent: 'center',
                transform: show && 'rotate(90deg)',
                transition: 'transform 0.25s ease',
              }}
            >
              <RightSVG />
            </div>
          }
          disabled={disabled}
        />
      ) : (
        <Input
          type="text"
          value={value || null}
          placeholder={placeholder || 'Select'}
          label={label || ''}
          readonly
          onClick={() => setShow((prev) => !prev)}
          suffixIcon={
            <div
              style={{
                display: 'grid',
                placeContent: 'center',
                transform: show && 'rotate(90deg)',
                transition: 'transform 0.25s ease',
              }}
            >
              <RightSVG />
            </div>
          }
          disabled={disabled}
        />
      )
      }

      {
        show && (
          <DrawerBase
            isOpen={show}
            onClose={() => setShow((prev) => !prev)}
            onBack={() => setShow((prev) => !prev)}
            title={title}
          >
            <div className={root}>
              {withSearch && (
                <div className={content_search}>
                  <Input
                    type="text"
                    placeholder={searchPlaceholder || 'Search...'}
                    small
                    prefixIcon={<SearchSVG />}
                    onChange={(e) => setSearchValue(e)}
                    value={searchValue}
                  />
                </div>
              )}
              <ul className={content_list}>
                {selections
                  ?.filter((dt) =>
                    dt.label.toLowerCase().includes(searchValue.toLowerCase())
                  )
                  .map((item, key) => {
                    return (
                      <li
                        onClick={() => {
                          onChange(key);
                          setSelected(item);
                          setShow(false);
                        }}
                      >
                        {item?.label}
                      </li>
                    );
                  })}
              </ul>
              {withConfirm && (
                <footer>
                  <Button variant="primary" size="full">
                    Confirm
                  </Button>
                </footer>
              )}
            </div>
          </DrawerBase>
        )
      }
    </div >
  );
};

export default DrawerSelectInput;
