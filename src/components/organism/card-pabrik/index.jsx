import { Button, Card, Divider } from 'components/atoms';

import { ReactComponent as LocationRedSVG } from 'assets/icons/location-red-icon.svg';

import styles from './styles.module.scss';

const {
  form_card,
  form_card__action,
  form_card__action_name,
  form_card__content,
  form_card__address,
} = styles;

const CardPabrik = (props) => {
  const { title, alamat, color, icon_color, selected, change = () => { }, } = props;

  return (
    <Card color={color}>
      <div className={form_card}>
        <div className={form_card__action}>
          <div className={form_card__content}>
            <div>
              <LocationRedSVG color={icon_color} />
            </div>
            <div className={form_card__action_name}>
              <h4>{title}</h4>
              {selected ? (
                <p className={form_card__address}>{alamat}</p>
              ) : (
                <p>{alamat}</p>
              )}
            </div>
            {selected && (
              <div className={form_card__action_name}>
                <p onClick={change}><u>Change</u></p>
              </div>
            )}
          </div>
        </div>
      </div>
    </Card>
  );
};

export default CardPabrik;
