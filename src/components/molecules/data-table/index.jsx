import { Pagination } from "components/atoms";

import { ReactComponent as SortSVG } from "assets/icons/sort-icon.svg";

import styles from "./styles.module.scss";

const {
  table,
  table_header__col,
  table_body,
  table_body__row,
  table_body__col,
  sort_btn,
} = styles;

const DataTable = (props) => {
  const { columns = "", data = ["", ""], loading = false } = props;

  return (
    <section className={table}>
      <header>
        {columns?.map((col, idx) => {
          const equalWidth = 100 / columns?.length;

          return (
            <div
              key={`h-col-${idx}`}
              className={table_header__col}
              style={{ width: col?.width || `${equalWidth}%` }}
            >
              {col?.sortable ? (
                <div className={sort_btn}>
                  <SortSVG />
                </div>
              ) : null}
              <p>{col.title}</p>
            </div>
          );
        })}
      </header>

      <article className={table_body}>
        {data?.map((item, index) => {
          const isEven = index % 2 !== 0;

          return (
            <div
              className={table_body__row}
              key={`b-row-${index}`}
              data-even={isEven}
            >
              {columns?.map((col, i) => {
                const equalWidth = 100 / columns?.length;
                return (
                  <div
                    key={`b-col-${i}`}
                    className={table_body__col}
                    style={{ width: col?.width || `${equalWidth}%` }}
                  >
                    {col?.render ? null : <p>Table {i}</p>}
                  </div>
                );
              })}
            </div>
          );
        })}
      </article>

      <footer>
        <Pagination />
      </footer>
    </section>
  );
};

export default DataTable;
