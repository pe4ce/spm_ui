import { useState } from 'react';
import { Button, Card } from 'components/atoms';
import { MobileLayout, FormLayout } from 'components/layout';
import { HeaderSearch, HeaderSelected } from 'components/molecules'
import { Input, SelectInput, SelectInputChange, DatePickerInput, ImageInput } from 'components/molecules';
import { CardPabrik } from 'components/organism';
import { DrawerSelectDate, DrawerSelectInput } from 'components/drawer';

import { ReactComponent as PlusSVG } from 'assets/icons/plus-icon.svg';
import { ReactComponent as MinusSVG } from 'assets/icons/minus-icon.svg';

import styles from './styles.module.scss';

const {
  root,
  form_root,
  form_container,
  list_container,
} = styles;

const FormOutputPabrik = (props) => {
  const { data = [{ nama: 'Asian Agri', alamat: 'Jl. M.H. Thamrin No.31, RT.4/RW.1, Gondangdia, Kecamatan Tanah Abang, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10230' }, { nama: 'PT. Pasifik Agro Sentosa', alamat: 'Sudirman Central Business District, RT.5/RW.3, Senayan, Kebayoran Baru, South Jakarta City, Jakarta 1219' }] } = props;
  const [selected, setSelected] = useState({ nama: '', alamat: '' });
  const [searchValue, setSearchValue] = useState('');
  const {
    onChange = () => { },
  } = props;

  return (
    <MobileLayout>

      {selected.nama !== '' ? (
        <section className={root}>
          <HeaderSelected
            title="Output Pabrik"
            nama={selected?.nama}
            alamat={selected?.alamat}
            onChange={() => {
              setSelected({ nama: '', alamat: '' });
            }} />
          <section className={form_root}>
            <div className={form_container}>
              <DrawerSelectInput
                label="Product type"
                placeholder="Choose product"
                title="Pilih Produk"
                selections={[
                  {
                    label: 'CPO',
                  },
                  {
                    label: 'Kernel',
                  },
                  {
                    label: 'Cangkang',
                  },
                ]}
                positionStatic />
            </div>
          </section>
        </section>
      ) : (
        <section className={root}>
          <HeaderSearch
            title="Output Pabrik"
            placeholder="Tuliskan nama pabrik"
            onChange={(e) => setSearchValue(e)}
            value={searchValue} />

          <article className={list_container}>

            <ul>
              {data
                ?.filter((dt) =>
                  dt.nama.toLowerCase().includes(searchValue.toLowerCase())
                )
                .map((item, key) => {
                  return (
                    <li
                      style={{ marginBottom: '2rem' }}
                      onClick={() => {
                        onChange(key);
                        setSelected(item);
                      }}
                    >
                      <CardPabrik
                        key={key}
                        title={item.nama}
                        alamat={item.alamat}
                        color="white"
                        icon_color="#919AAB" />
                    </li>
                  );
                })}
            </ul>

          </article>
        </section>
      )
      }
    </MobileLayout >
  );
};

export default FormOutputPabrik;
