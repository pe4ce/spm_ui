import React, { useEffect } from 'react';

import styles from './styles.module.scss';

const { step_container, dash, step_dots } = styles;

const getStepClass = (step, current) => {
  const classes = [styles['step_round']];

  if (step < current) classes.push(styles['done']);
  if (step === current) classes.push(styles['current']);

  return classes.filter(Boolean).join(' ');
};

const Stepper = (props) => {
  const { length, current, setStep } = props;
  const steps = Array.from(Array(length).keys());

  return (
    <section style={{ width: '100%' }}>
      <div
        style={{
          display: 'flex',
          //   gridTemplateColumns: `repeat(${length + (length - 1)}, 1fr)`,
          alignItems: 'center',
          justifyContent: 'center',
          textAlign: 'center',
          width: '100%',
        }}
      >
        {steps.map((step, idx) => {
          const stepClass = getStepClass(step, current);
          if (idx !== length - 1) {
            return (
              <>
                <div className={step_container}>
                  <div onClick={() => setStep(step)} className={stepClass}>
                    <span className={step_dots}></span>
                  </div>
                </div>{' '}
                <hr className={dash} />
              </>
            );
          } else {
            return (
              <div className={step_container}>
                <div onClick={() => setStep(step)} className={stepClass}>
                  <span className={step_dots}></span>
                </div>
              </div>
            );
          }
        })}
        {/* {JSON.stringify(steps)} */}
      </div>
    </section>
  );
};

export default Stepper;
