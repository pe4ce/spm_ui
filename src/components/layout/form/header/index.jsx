import { PageNavigationTitle, Stepper } from 'components/molecules';
import styles from './styles.module.scss';

const { root, stepper_container } = styles;

const FormHeader = ({
  title,
  onBack,
  secondActionElement,
  steps,
  currentStep,
  component,
  setStep,
}) => {
  return (
    <header className={root}>
      <PageNavigationTitle
        title={title}
        onBack={onBack}
        secondActionElement={secondActionElement}
        small
      />
      {steps ? (
        <div className={stepper_container}>
          {/* <p>this is stepper</p> */}
          <Stepper length={steps} current={currentStep} setStep={setStep} />
        </div>
      ) : (
        component || null
      )}
    </header>
  );
};

export default FormHeader;
