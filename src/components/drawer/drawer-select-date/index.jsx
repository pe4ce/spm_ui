import { Button, DatePickerBase } from 'components/atoms';
import { DrawerBase, Input } from 'components/molecules';
import { useState } from 'react';

import { format } from 'date-fns';

import { ReactComponent as CalendarSVG } from 'assets/icons/calendar-icon.svg';

import styles from './styles.module.scss';

const { root } = styles;

const DrawerSelectDate = (props) => {
  const { label, placeholder } = props;
  const [selectedDate, setSelectedDate] = useState('');
  const [dateString, setDateString] = useState('');
  const [show, setShow] = useState(false);

  return (
    <div>
      <Input
        type="text"
        readonly
        prefixIcon={<CalendarSVG />}
        placeholder={placeholder || 'Select date'}
        onClick={() => setShow((prev) => !prev)}
        value={dateString}
        label={label}
      />

      {
        show && (
          <DrawerBase
            isOpen={show}
            onClose={() => setShow((prev) => !prev)}
            onBack={() => setShow((prev) => !prev)}
            title="Date Plan"
            secondAction={
              <Button
                onClick={() => {
                  setSelectedDate(new Date());
                  setDateString(format(new Date(), 'dd/MM/yyyy'));
                  setShow(false);
                }}
                size="small">
                Today
              </Button>
            }
          >
            <div className={root}>
              <DatePickerBase
                value={selectedDate}
                setValue={(v) => {
                  setSelectedDate(v);
                  setDateString(format(v, 'dd/MM/yyyy'));
                  setShow(false);
                }} />
            </div>
          </DrawerBase>
        )
      }
    </div>
  );
};

export default DrawerSelectDate;
