import { MobileLayout, FormLayout } from 'components/layout';
import { Input, SelectInput } from 'components/molecules';
import { DrawerSelectDate, DrawerSelectInput } from 'components/drawer';

import { ReactComponent as PlusSVG } from 'assets/icons/plus-icon.svg';
import { ReactComponent as MinusSVG } from 'assets/icons/minus-icon.svg';
import { ReactComponent as DownSVG } from 'assets/icons/chevron-down-icon.svg';

import styles from './styles.module.scss';

const {
  form_root,
  form_container,
  form_btn_container,
  form_product__container,
  form_product__list,
  input_number_btn,
  product_input,
} = styles;

const FormDeliveryOrder = () => {


  return (
    <MobileLayout>
      <FormLayout
        title="Delivery Order"
        okText="Submit Form"
        onOk={() => console.log('save')}
        onCancel={() => { }}
      >
        <section className={form_root}>
          <div className={form_container}>
            <DrawerSelectInput
              label="Delivery Order"
              placeholder="Pilih delivery order"
              title="Pilih Delivery Order"
              selections={[
                {
                  label: 'DO 124 G',
                },
                {
                  label: 'DO 124 C',
                },
                {
                  label: 'DO 124 W',
                },
              ]}
              positionStatic
              withSearch
              searchPlaceholder="Tuliskan deliver order"
              withBack />
            <DrawerSelectDate label="Tanggal" placeholder="Pilih tanggal" />

            <div className={form_product__container}>
              <p>Produk</p>
              <div className={product_input}>
                { }
              </div>
            </div>

            <Input
              type="text"
              label="Nama Logistic"
              placeholder="Tuliskan nama logistic"
            />

            <Input
              type="text"
              label="Plat Nomor"
              placeholder="Tuliskan plat nomor"
            />

            {/* <ImageInput /> */}
          </div>
        </section>
      </FormLayout>
    </MobileLayout>
  );
};

export default FormDeliveryOrder;

