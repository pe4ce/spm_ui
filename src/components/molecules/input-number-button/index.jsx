import { ReactComponent as PlusSVG } from 'assets/icons/plus-icon.svg';
import { ReactComponent as MinusSVG } from 'assets/icons/minus-icon.svg';

import styles from './styles.module.scss';

const { input_number_btn } = styles;

const InputNumberButton = (props) => {
  const { onPlus = () => { }, onMinus = () => { }, label = '', required = false, hideLabel = false, satuan = '' } = props;

  return (
    <div>
      {label ? (
        <p style={{ visibility: hideLabel ? 'hidden' : 'visible' }}>
          {label} {required && <span>*</span>}
        </p>
      ) : null}
      <div className={input_number_btn}>
        <button onClick={onPlus}>
          <PlusSVG />
        </button>
        <input
          type="number"
          value={0}
          readOnly
          style={{ border: 'none', borderColor: 'transparent', textAlign: 'center' }}
        />{satuan || ''}
        <button onClick={onMinus}>
          <MinusSVG />
        </button>
      </div>
    </div>
  );
};

export default InputNumberButton;
