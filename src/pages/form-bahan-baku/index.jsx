import { useState } from "react";
import { Card } from "components/atoms";
import { MobileLayout, FormLayout } from "components/layout";
import {
  Input,
  SelectInput,
  InputNumberButton,
  PageTitle,
  ImageInput,
} from "components/molecules";
import { DrawerSelectDate, DrawerSelectInput } from "components/drawer";

import { ReactComponent as PlusSVG } from "assets/icons/plus-icon.svg";
import { ReactComponent as MinusSVG } from "assets/icons/minus-icon.svg";
import { ReactComponent as DownSVG } from "assets/icons/chevron-down-icon.svg";
import { ReactComponent as CalendarSVG } from "assets/icons/calendar-icon.svg";

import styles from "./styles.module.scss";

const { root, page_header, form_container, input_container, input_two } =
  styles;

const FormBahanBaku = () => {
  const [step, setStep] = useState(0);

  const StepOne = (
    <div className={form_container}>
      <DrawerSelectInput
        label="Pemasok"
        placeholder="Pilih pemasok"
        title="Pilih Pemasok"
        selections={[
          {
            label: "Perusahaan G",
          },
          {
            label: "Perusahaan H",
          },
          {
            label: "Perusahaan I",
          },
        ]}
        positionStatic
        withSearch
        searchPlaceholder="Tuliskan nama pemasok"
        suffixIcon={<u>Change</u>}
      />
      <DrawerSelectDate label="Tanggal" placeholder="Pilih tanggal" />
    </div>
  );

  const StepTwo = (
    <div className={form_container}>
      <div className={input_container}>
        <InputNumberButton label="Jumlah yang diterima" satuan="/kg" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="Jumlah yang diterima" satuan="/buah" />
      </div>

      <div className={input_container}>
        <InputNumberButton label="Jumlah yang diterima" satuan="/kg" />
      </div>

      <Input
        type="text"
        label="Plat Nomor"
        placeholder="Tulis nomor plat kendaraan"
      />
    </div>
  );
  const StepThree = (
    <div className={form_container}>
      <h5 style={{ marginBottom: -20 }}>Foto Penerimaan Barang</h5>
      <ImageInput />
    </div>
  );

  const stepComponent = {
    0: StepOne,
    1: StepTwo,
    2: StepThree,
  };

  return (
    <MobileLayout>
      <FormLayout
        title="Penerimaan Bahan Baku"
        okText="Submit Form"
        onOk={() => {
          if (step >= 2) {
            return;
          } else {
            setStep((st) => st + 1);
          }
        }}
        onCancel={() => {}}
        steps={3}
        setStep={setStep}
        currentStep={step}
      >
        <section className={root}>
          <Card color="white">{stepComponent[step]}</Card>
        </section>
      </FormLayout>
    </MobileLayout>
  );
};

export default FormBahanBaku;
