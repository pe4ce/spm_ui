export const joinClasses = (arrayOfClass = []) => {
  return arrayOfClass.filter(Boolean).join(' ');
};

export const trucateText = (string = '', limit = 20) => {
  return string.slice(0, limit) + '...';
};
