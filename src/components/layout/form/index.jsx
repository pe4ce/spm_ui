import FormHeader from './header';
import FormFooter from './footer';

import styles from './styles.module.scss';

const { root, content, container } = styles;

const FormLayout = ({
  children,
  title,
  okText,
  onOk,
  onCancel,
  cancelText,
  secondActionElement = null,
  steps,
  currentStep,
  setStep,
}) => {
  return (
    <section className={root}>
      <section className={container}>
        <FormHeader
          title={title}
          onBack={onCancel}
          secondActionElement={secondActionElement}
          steps={steps}
          currentStep={currentStep}
          setStep={setStep}
        />
        <article className={content}>{children}</article>
        <FormFooter
          okText={okText}
          onOk={onOk}
          cancelText={cancelText}
          steps={steps}
          currentStep={currentStep}
        />
      </section>
    </section>
  );
};

export default FormLayout;
