import { useState } from 'react';
import { format } from 'date-fns';

import { Input } from 'components/molecules';
import { Button, DatePickerBase, Divider } from 'components/atoms';

import { ReactComponent as CalendarSVG } from 'assets/icons/calendar-icon.svg';

import styles from './styles.module.scss';

const { root, datepicker } = styles;

const InputDateOicker = (props) => {
  const { label, placeholder } = props;
  const [selectedDate, setSelectedDate] = useState('');
  const [dateString, setDateString] = useState('');
  const [show, setShow] = useState(false);

  return (
    <div className={root}>
      <Input
        type="text"
        readonly
        prefixIcon={<CalendarSVG />}
        placeholder={placeholder || 'Select date'}
        onClick={() => setShow((prev) => !prev)}
        value={dateString}
        label={label}
      />

      {show && (
        <div className={datepicker}>
          <DatePickerBase
            value={selectedDate}
            setValue={(v) => {
              setSelectedDate(v);
              setDateString(format(v, 'MM/dd/yyyy'));
              setShow(false);
            }}
          />
          <footer>
            <Button
              size="full"
              onClick={() => {
                setSelectedDate(new Date());
                setDateString(format(new Date(), 'MM/dd/yyyy'));
                setShow(false);
              }}
            >
              Today
            </Button>
          </footer>
        </div>
      )}
    </div>
  );
};

export default InputDateOicker;
