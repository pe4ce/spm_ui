import { storageKeys } from 'libs/keys';
import React, { lazy, useEffect } from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router';
import { FormLayout, MainLayout } from 'components/layout';

import { FormListPage, FormBahanBaku, FormOutputPabrik, FormQualityControl, FormDeliveryOrder } from 'pages';

// const CreateCustomerOrderPage = lazy(() =>
//   import('pages/customer-order/create')
// );
// const CustomerOrderPage = lazy(() => import('pages/customer-order'));

const formRoutes = [
  {
    name: 'Mobile Form List',
    component: FormListPage,
    path: '/form-list',
    exact: true,
  },
  {
    name: 'Mobile Form Bahan Baku',
    component: FormBahanBaku,
    path: '/form/penerimaan-bahan-baku',
    exact: true,
  },
  {
    name: 'Mobile Form Output Pabrik',
    component: FormOutputPabrik,
    path: '/form/output-pabrik',
    exact: true,
  },
  {
    name: 'Mobile Form Quality Control',
    component: FormQualityControl,
    path: '/form/quality-control',
    exact: true,
  },
  {
    name: 'Mobile Form Delivery Order',
    component: FormDeliveryOrder,
    path: '/form/delivery-order',
    exact: true,
  },
];

const FormRoutes = () => {
  return (
    <Switch>
      {formRoutes.map((route, index) => {
        return (
          <Route
            key={index}
            exact={route.exact}
            path={route.path}
            component={route.component}
          />
        );
      })}

      {/* <Route
        exact
        path="/"
        render={() => {
          // const token = getUserToken(storageKeys.token) || null;
          const token = true;

          if (token) {
            return <Redirect to="/form-list" />;
          } else {
            return <Redirect to="/signin" />;
          }
        }}
      /> */}
    </Switch>
  );
};

export default FormRoutes;
