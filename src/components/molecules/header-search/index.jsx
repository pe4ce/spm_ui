import styles from './styles.module.scss';
import { PageNavigationTitle } from 'components/molecules';
import { Input } from 'components/molecules';

import { ReactComponent as LocationRedSVG } from 'assets/icons/location-red-icon.svg';

const { header, root_search } = styles;

const HeaderSearch = (props) => {
    const {
        placeholder = '',
        onChange = () => { },
        title = '',
        value = '',
        secondActionElement = '',
    } = props;

    return (
        <header className={header}>
            <div>
                <PageNavigationTitle
                    title={title}
                    onBack={onChange}
                    secondActionElement={secondActionElement}
                    small
                />
            </div>
            <div className={root_search}>
                <Input
                    type="text"
                    placeholder={placeholder}
                    prefixIcon={<LocationRedSVG color={"#EF6D3F"} />}
                    onChange={onChange}
                    value={value}
                />
            </div>
        </header>
    );
};

export default HeaderSearch;
