import { Button, Card, Divider } from 'components/atoms';

import { ReactComponent as ChevronRightSVG } from 'assets/icons/chevron-right-icon.svg';

import styles from './styles.module.scss';

const {
  form_card,
  form_card__action,
  form_card__action_name,
  form_card__information,
  form_card__icon,
} = styles;

const FormCard = (props) => {
  const { title, action = () => { }, lastSubmit } = props;

  return (
    <Card color="white">
      <div className={form_card}>
        <div className={form_card__action}>
          <div className={form_card__action_name}>
            <div className={form_card__icon}></div>
            <h4 style={{ fontSize: '2rem' }}>{title}</h4>
          </div>
          <Button onClick={action}>
            <ChevronRightSVG />
          </Button>
        </div>
        <Divider grey />
        <div className={form_card__information}>
          <p>Last Submited: {lastSubmit}</p>
        </div>
      </div>
    </Card>
  );
};

export default FormCard;
